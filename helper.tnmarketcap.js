jQuery( document ).ready(function() {
	const lang = jQuery('html')[0].lang || "en-US"; // Get the language
	
	// Format the currency
    const currencyFormatter = new Intl.NumberFormat(lang, { style: 'currency', currency: 'TND' });
	const priceDomElement = jQuery('#tkr_share_price');
	if (!priceDomElement.html()) return; // Protection in case there is no data
	priceDomElement.html(currencyFormatter.format(priceDomElement.html()));
	
	// Format the date
	const dateFormatter = new Intl.DateTimeFormat(lang, { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' });
	const dateDomElement = jQuery('#tkr_date');
	let rawDate = dateDomElement.html();
	const fr_months = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
	const en_months = ["January","February","March","April","May","June","July", "August","September","October","November","December"];
	fr_months.map((month, key) => {
		rawDate = rawDate.replace(month, en_months[key]);
	});
	dateDomElement.html(dateFormatter.format(new Date(rawDate)));
});