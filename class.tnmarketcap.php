<?php
class wp_tnmarketcap_plugin extends WP_Widget {
	function  wp_tnmarketcap_plugin() {
		parent::WP_Widget(false, $widget_name = __('Tn Market Cap', 'tkr_tnmaketcap') );
	}

	function form($instance) {
		if ($instance) {
			$isin = esc_attr($instance['isin']);
		} else {
			$isin = '';
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id('isin'); ?>"><?php _e('ISIN:', 'tkr_tnmaketcap'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('isin'); ?>" name="<?php echo $this->get_field_name('isin'); ?>" type="text" value="<?php echo $isin; ?>" />
		</p>
		<?php
	}

	// Update function update the insert value in input field in DB
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['isin'] = strip_tags($new_instance['isin']);
		return $instance;
	}
	
	// Get the data from ISIN code
	function getData($isin) {
		$data = get_transient('tnmarketcap_'.$isin);
		$last_refrech = get_transient('tnmarketcap_'.$isin.'_last_refrech');
		if ( $data === false || $last_refrech === false ) {
			$date = new DateTime();
			$request = wp_remote_get( 'http://www.bvmt.com.tn/rest_api/rest/market/'.$isin );
			if (is_wp_error($request) || !isset($request['body'])) { 
				set_transient('tnmarketcap_'.$isin.'_last_refrech', $date->getTimestamp(), 1 * HOUR_IN_SECONDS);
				return null;
			}
			$data = json_decode($request['body'])->market;
			set_transient('tnmarketcap_'.$isin.'_last_refrech', $date->getTimestamp(), 1 * HOUR_IN_SECONDS);
			set_transient('tnmarketcap_'.$isin, $data, 3 * MONTH_IN_SECONDS);
		}
		return $data;
	}
	
	// Render the widget
	function widget($args, $instance) {
		extract( $args );
		$isin = $instance['isin'];
		if (!$isin) return null;
		
		$data = $this->getData($isin);
		if ($data === null) return '';
		
		echo $before_widget;
		echo '<div class="widget-text tkr_tnmarketcap_widget">';
		if( $isin ) {
			echo '<p class="wp_widget_plugin_text">';
			echo sprintf(__( '<span id="tkr_sentence"><span id="tkr_company">%s</span> share price (<span id="tkr_date">%s</span>): </span>', 'tkr_tnmaketcap' ), $data->referentiel->ticker, $data->seance.' '.$data->time );
			echo '<span class="share_price" id="tkr_share_price">'.$data->last.'</span></p>';
		}
		echo '</div>';
		echo $after_widget;
	}
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("wp_tnmarketcap_plugin");'));