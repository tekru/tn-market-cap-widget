=== Tn Market Capital Widget ===

Contributors: Mohamed Kharrat <kharrat.mohamed@gmail.com>
Tags: Tunisian Market, Bourse de Tunis
Requires at least: 4.0
Tested up to: 4.8
Stable tag: trunk
Requires PHP: 5.4
License: Creative commun
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A plugin that create a widget to show the share price of a Tunisian publicly traded company in the Tunis Stock Exchange market (Bourse de Tunis).

== Description ==

A plugin that create a widget to show the share price of a Tunisian publicly traded company in the Tunis Stock Exchange market (Bourse de Tunis).

Based on the officiel Bourse de Tunis API, the plugin calls the API every hour to get an updated price and caches it in the Wordpress.

This plugin was made with no style includes. You'll have to add your own CSS using the classes and ids available in the widget.

This widget gets the data based on the ISIN of a company.

Made by the team of [Tekru Technologies](http://tekru.net/ "Tekru Technologies web site").

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Add the widget to your widget area and set the company ISIN

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

2020-06-13  Initiale commit.

== Upgrade Notice ==

