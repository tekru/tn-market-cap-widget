<?php
/**
 * @package TN Market Cap
 * @version 0.1
 */
/*
Plugin Name: Tunisian Market Cap
Description: A tunisian market capital widget for Wordpress
Version: 0.1
Author: Tekru Technologies
Author URI: https://www.tekru.net/
Text Domain: tkr_tnmaketcap
*/
// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there! At Tekru Technologies we made this code to nbe just a plugin, not much I can do when called directly.';
	exit;
}

define('TNMARKETCAP__PLUGIN_DIR', plugin_dir_path( __FILE__ ));

function load_tnmarketcap_helper(){
    wp_register_script( 
        'tnmarketcap_helper', 
        plugin_dir_url( __FILE__ ) . 'helper.tnmarketcap.min.js', 
        array( 'jquery' )
    );
    wp_enqueue_script( 'tnmarketcap_helper' );
}
add_action('wp_enqueue_scripts', 'load_tnmarketcap_helper');

require_once(TNMARKETCAP__PLUGIN_DIR . 'class.tnmarketcap.php');
